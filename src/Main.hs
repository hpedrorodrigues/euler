module Main where

import qualified Data.Map as DataMap
import System.Environment (getArgs)
import System.Exit (exitSuccess)
import ProjectEuler.Problem001
import ProjectEuler.Problem002

solutions :: DataMap.Map Integer Integer
solutions = DataMap.fromList [
  (1, solution001),
  (2, solution002)]

solution :: Integer -> Maybe Integer
solution number = DataMap.lookup number solutions

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["--help"] -> usage >> exitSuccess
        ["-h"] -> usage >> exitSuccess
        [number] -> case solution (read number :: Integer) of
            Just result -> putStrLn ("Solution for problem " ++ number ++ " is: " ++ (show result))
            Nothing -> putStrLn "There is no solution yet for this problem"
        _ -> usage >> exitSuccess
    where
        usage = putStrLn "Usage: cabal run problem [number]"