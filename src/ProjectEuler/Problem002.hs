module ProjectEuler.Problem002 (solution002) where

limit :: Integer
limit = 4000000

isEven :: Integer -> Bool
isEven x = x `mod` 2 == 0

fibonacci :: Integer -> Integer
fibonacci 1 = 1
fibonacci 2 = 2
fibonacci n = fibonacci(n - 1) + fibonacci(n - 2)

recursiveSum :: Integer -> Integer -> Integer
recursiveSum xs x = if (value > limit) then xs else recursiveSum (xs + validValue) (x + 1)
    where value = fibonacci x
          validValue = if isEven value then value else 0

solution002 :: Integer
solution002 = (recursiveSum 0 1)