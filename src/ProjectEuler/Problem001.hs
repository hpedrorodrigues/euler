module ProjectEuler.Problem001 (solution001) where

solution001 :: Integer
solution001 = sum [ x | x <- [1..999], x `mod` 3 == 0 ||  x `mod` 5 == 0 ]